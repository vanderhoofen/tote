FROM docker:git

#        make \
RUN apk add --no-cache --update \
        ca-certificates \
        curl \
        python3 \
        tar && \
    pip install --upgrade pip && \
    pip3 install -U pip \
        flake8 \
        tote 
